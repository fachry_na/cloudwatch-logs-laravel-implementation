<?php

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    Log::channel('cloudwatch')->debug(
        'test',
        [
            "ahead" => 496507751.92613983,
            "soon" => 1768824629,
            "upon" => "strong",
            "over" => 599440530,
            "sick" => "facing",
            "report" => -946742837,
            "writing" => "fifteen",
            "make" => -1202935302.6801348,
            "establish" => 1423086382,
            "kind" => 1659199286,
            "teach" => "break",
            "me" => "bite",
            "longer" => "shore",
            "wonder" => -678793795,
            "whose" => "hung",
        ]
    );
    return response()->json([
        'message' => 'ok',
        'apps' => config('app.name'),
    ]);
});
