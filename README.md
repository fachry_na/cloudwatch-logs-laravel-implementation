<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## Tentang Project

Project ini adalah contoh integrasi [CloudWatch Logs](https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/WhatIsCloudWatchLogs.html) dengan Laravel. Fitur yang diimplementasikan pada project ini hanyalah fitur untuk menulis `log event` ke `CloudWatch Log Stream`.

## Integrasi CloudWatch

Dalam integrasi CloudWatch Logs diperlukan user IAM yang memiliki _permission_ sebagai berikut:

-   `logs:DescribeLogGroups` (Mengizinkan pengguna untuk melihat semua `Log Group` yang terasosiasi dengan akun)
-   `logs:DescribeLogStreams` (Mengizinkan pengguna untuk melihat semua `Log Stream` yang terasosiasi dengan akun)
-   `logs:CreateLogGroup` (Mengizinkan pengguna untuk membuat `Log Group`)
-   `logs:DeleteLogGroup` (Mengizinkan pengguna untuk menghapus `Log Stream`)
-   `logs:CreateLogStream` (Mengizinkan pengguna untuk membuat `Log Stream`)
-   `logs:DeleteLogStream` (Mengizinkan pengguna untuk menghapus `Log Stream`)
-   `logs:PutLogEvents` (Mengizinkan pengguna untuk menyimpan log ke dalam `Log Stream`)
-   `logs:PutRetentionPolicy` (Mengizinkan pengguna untuk menentukan waktu retensi dari log)
-   `logs:GetLogEvents` (Mengizinkan pengguna untuk melihat semua `log event` yang ada pada `Log Stream`)
-   `logs:FilterLogEvents` (Mengizinkan pengguna untuk melihat `log event` yang ada pada `Log Group` berdasarkan filter yang diberikan)
-   `logs:StartQuery` (Mengizinkan pengguna untuk memulai `CloudWatch Logs Insights queries`)
-   `logs:StopQuery` (Mengizinkan pengguna untuk menghentikan `query` yang sedang berjalan pada pada `CloudWatch Logs Insights`)

Akses-akses tersebut akan mempermudah kita saat melakukan _testing_ serta _monitoring_ `log event` yang tercatat.

### Membuat Log Group dan Log Stream

Untuk membuat Log Group langkah-langkah yang diperlukan adalah:

1. Login AWS menggunakan user IAM yang telah diberikan permission diatas
2. Menuju ke halaman [CloudWatch](https://console.aws.amazon.com/cloudwatch/home)
3. Menuju ke halaman `Log Groups` dengan memilih dropdown `Logs` pada _sidebar_ lalu menekan `Log Groups`. Anda akan diarahkan ke halaman daftar `Log Groups` yang terasosiasi dengan akun anda.
4. Tekan tombol `Create log group` untuk membuat `Log Group`.

    ![Tombol Create log group](/public/readme/image.png)

5. Isi nama `Log Group` yang diinginkan serta berapa lama `Log Events` ingin disimpan yang dapat diisikan pada kolom `Retention Setting`

    ![Tampilan form untuk membuat Log Group](/public/readme/image-1.png)

6. Tekan tombol `Create`

Untuk membuat Log Stream langkah-langkah yang diperlukan adalah:

1. Pilih `Log Group` yang telah dibuat sebelumnya
2. Pada tampilan detail `Log Group` terdapat tab `Log Streams`

    ![Halaman detail Log Group](/public/readme/image-2.png)

3. Tekan tombol `Create log stream` dan anda akan ditampilkan _popup_ untuk memasukkan nama `Log Stream`.

    ![Popup pembuatan log stream](/public/readme/image-3.png)

4. Setelah memasukkan nama tekan tombol `Create` dan `Log Stream` pun berhasil dibuat.
5. Anda dapat melihat `Log Events` yang tercatat dengan menekan salah satu `Log Stream`.

### Integrasi _CloudWatch Log_ dengan Laravel

Agar kita dapat menulis log yang kita inginkan sebagai `Log Event` pada `CloudWatch Logs` beberapa langkah yang perlu dilakukan adalah sebagai berikut:

1. Install library [aws/aws-sdk-php](https://github.com/aws/aws-sdk-php) dengan command:
    ```
        composer require aws/aws-sdk-php:^3.0
    ```
2. Tambahkan beberapa properti serta nilai pada `.env`. Properti yang diperlukan adalah:
    - `CLOUDWATCH_GROUP` (untuk menentukan Log Group apa yang akan kita gunakan)
    - `CLOUDWATCH_STREAM` (untuk menentukan Log Stream apa yang akan kita gunakan)
    - `CLOUDWATCH_REGION` (untuk menentukan region AWS yang kita gunakan)
    - `CLOUDWATCH_ACCESS_KEY_ID` (_Access key_ dari akun IAM yang telah diberikan akses)
    - `CLOUDWATCH_SECRET_ACCESS_KEY` (_Secret key_ dari akun IAM yang telah diberikan akses)
3. Tambahkan _logging channel_ baru pada `config/logging.php` dengan konfigurasi sebagai berikut:
    ```
        'cloudwatch' => [
            'driver' => 'monolog',
            'handler' => \App\Logging\CloudwatchLogFactories::class,
            'formatter' => \Monolog\Formatter\JsonFormatter::class,
            'name' => 'fachry-test',
            'with' => [
                'group' => env('CLOUDWATCH_GROUP', 'test'),
                'stream' => env('CLOUDWATCH_STREAM', 'test'),
                'region' => env('CLOUDWATCH_REGION', 'ap-southeast-1'),
                'version' => env('CLOUDWATCH_VERSION', 'latest'),
                'level' => env('CLOUDWATCH_LOG_LEVEL', \Monolog\Logger::DEBUG),
                'credentials' => [
                    'key' => env('CLOUDWATCH_ACCESS_KEY_ID'),
                    'secret' => env('CLOUDWATCH_SECRET_ACCESS_KEY')
                ],
            ],
        ],
    ```
4. Copy folder `app/Logging` ke proyek anda

### Penggunaan _CloudWatch Log_ yang telah diimplementasikan

Untuk menulis log kedalam bentuk `Log Events` pada `CloudWatch Log` dapat dilakukan dengan 2 cara yaitu:

1. Dengan mengatur nilai `LOG_CHANNEL` menjadi `cloudwatch`:
    ```
        LOG_CHANNEL=cloudwatch
    ```
2. Secara eksplisit menggunakan channel `cloudwatch` saat menggunakan fungsi logging:
    ```
    Log::channel('cloudwatch')->debug(
        'test',
        [
            "ahead" => 496507751.92613983,
            "soon" => 1768824629,
            "upon" => "strong",
            "over" => 599440530,
            "sick" => "facing",
        ]
    );
    ```

### Contoh Hasil Log yang Berhasil Tercatat
1. Tampilan pada halaman `Log Stream`:
    ![Tampilan daftar log pada Log Stream](/public/readme/image-4.png)
2. Tampilan saat dilakukan filter pada `Log Insights`:
    ![Tampilan saat dilakukan filter pada Log Insights](/public/readme/image-5.png)


## Catatan
Masih ada beberapa kekurangan dari repo ini antara lain:
1. Belum bisa menerapkan pembuatan `Log Group` dan `Log Stream` secara dinamis.
2. Belum dioptimasi untuk 1 log yang sizenya besar
3. Belum ada pengecekan apakah ukuran lognya lebih besar dari limit yang diberikan