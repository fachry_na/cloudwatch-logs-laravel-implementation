<?php

namespace App\Logging;

use Aws\CloudWatchLogs\CloudWatchLogsClient;
use Aws\CloudWatchLogs\Exception\CloudWatchLogsException;
use Aws\Credentials\Credentials;
use Monolog\Formatter\FormatterInterface;
use Monolog\Formatter\HtmlFormatter;
use Monolog\Formatter\JsonFormatter;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;

class CloudwatchLogFactories extends AbstractProcessingHandler
{
    /**
     * @var CloudWatchLogsClient
     */
    private $client;

    private string $group;

    private string $stream;

    private array $records = [];

    public function __construct(
        string $group,
        string $stream,
        array $credentials,
        $version,
        string $region,
        int $level = Logger::DEBUG,
        $bubble = true,
    ) {
        $sdk['region'] = $region;
        $sdk['version'] = $version;
        $sdk['credentials'] = new Credentials($credentials['key'], $credentials['secret']);
        try {
            // Instantiate AWS SDK CloudWatch Logs Client
            $client = new CloudWatchLogsClient($sdk);
        } catch (CloudWatchLogsException $e) {
            throw $e;
        }

        $this->client = $client;
        $this->group = $group;
        $this->stream = $stream;

        parent::__construct($level, $bubble);
    }

    protected function write(array $record): void
    {
        $records = $this->formatRecords($record);
        $this->records[] = $records;
    }

    private function formatRecords(array $entry): array
    {
        $timestamp = $entry['datetime']->format('U.u') * 1000;

        $messages = [
            "type" => $entry["level_name"],
            "channel" => $entry["channel"],
            "title" => $entry["message"],
            "data" => $entry["context"],
        ];

        $messages = json_encode($messages);

        $records = [
            'message' => $messages,
            'timestamp' => $timestamp
        ];

        return $records;
    }

    private function send(): void
    {
        $entries = $this->records;

        $data = [
            'logGroupName' => $this->group,
            'logStreamName' => $this->stream,
            'logEvents' => $entries
        ];
        
        $this->client->putLogEvents($data);
    }

    public function close(): void
    {
        $this->send();
    }
}
